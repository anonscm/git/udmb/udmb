package net.meddeb.udmb.system;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/



import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;

import net.meddeb.bee.BeeMissingChannelException;
import net.meddeb.bee.Beepin;

/**
 * Session Bean implementation class ChannelTester
 */
@Stateless
@LocalBean
public class ChannelTester {
	private Logger logger = Logger.getLogger(this.getClass());
	@EJB
	private Beepin beepin;
	
	private boolean messageReceived = false;
	
	private class TestListener implements MessageListener{

		@Override
		public void onMessage(Message message) {
			try {
				logger.info("== * Message " + ((TextMessage)message).getText() + " successfully received by listener.");
				logger.info("==");
				logger.info("======== CHANNEL TEST END ==========================");
				messageReceived = true;
			} catch (JMSException e) {
				logger.error("Error wen testing channel " + e.getMessage());
			}
		}
		
	}

	public ChannelTester() {
	}
	
	@SuppressWarnings("finally")
	public boolean isChannelWork(String channelID){
		boolean rslt = false;
		logger.info("======== CHANNEL TEST START ==========================");
		logger.info("== Channel ID: " + channelID);
		logger.info("==");
		try {
			rslt = beepin.isChannelStarted(channelID);
			if (rslt){
				logger.info("== * Channel is running, create listener.. ");
				beepin.addListener(channelID, new TestListener());
				logger.info("== * Send test message: " + "TEST-TEST-TEST");
				beepin.sendMessage(channelID, "REQ-TEST-PWDQP", "TEST-TEST-TEST");
			}
		} catch (BeeMissingChannelException e) {
			rslt = false;
			logger.error(e.getMessage() + ": " + channelID);
			logger.info("== * Error occured: " + e.getMessage());
			logger.info("==");
			logger.info("======== CHANNEL TEST END ==========================");
		} catch (Exception e) {
			rslt = false;
			logger.error("Error wen testing channel " + channelID + ": " +e.getMessage());
			logger.info("== * Error occured: " + e.getMessage());
			logger.info("==");
			logger.info("======== CHANNEL TEST END ==========================");
		} finally{
			return rslt;
		}
	}

	public boolean isMessageReceived() {
		return messageReceived;
	}

}
