package net.meddeb.udmb.system;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.util.ArrayList;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import net.meddeb.bee.ChannelType;
import net.meddeb.udmb.shared.ChannelInfo;

/**
 * Session Bean implementation class Parameters
 */
@Singleton
@LocalBean
public class Parameters {
	private boolean initialized = false;
	private ArrayList<ChannelInfo> channelList = null;
	private int channelTypeToInt(ChannelType channelType){
		switch (channelType){
			case LOCALCHANNEL:
				return 0;
		case FOREIGNCHANNEL:
				return 1;
		case QUEUECONTROLCHANNEL:
				return 2;
		default:
				return -1;
		}
	}
	private void initilize(){
		if (initialized) return;
		//Password quality parameters
		channelList.clear();
		ChannelInfo channel = new ChannelInfo();
		channel.setID("pwdQualityParams");
		channel.setDescription("Password quality parameters");
		channel.setChannelType(channelTypeToInt(ChannelType.FOREIGNCHANNEL));
		channel.setLocal(false);
		String[] messageTypes = {"REQ-READ-PWDQP","RES-READ-PWDQP","REQ-WRITE-PWDQP","RES-WRITE-PWDQP","REQ-TEST-PWDQP"};
		channel.setMessageTypes(messageTypes);
		channelList.add(channel);
		initialized = true;
	}

	public Parameters() {
		if (channelList == null) channelList = new ArrayList<ChannelInfo>();
		initilize();
  }
	public ChannelType intToChannelType(int ichannelType){
		switch (ichannelType){
			case 0:
				return ChannelType.LOCALCHANNEL;
			case 1:
				return ChannelType.FOREIGNCHANNEL;
			case 2:
				return ChannelType.QUEUECONTROLCHANNEL;
			default:
				return null;
		}
	}
	
	public int channelCount(){
		return channelList.size();
	}
	
	public String getChannelID(int idx){
		return channelList.get(idx).getID();
	}
	
	public String getChannelDescription(int idx){
		return channelList.get(idx).getDescription();
	}
	
	public ChannelType getChannelType(int idx){
		return intToChannelType(channelList.get(idx).getChannelType());
	}
	
	public String[] getMessageTypes(int idx){
		return channelList.get(idx).getMessageTypes();
	}
	
	public ChannelInfo getChannelInfo(int idx){
		return channelList.get(idx);
	}
}
