package net.meddeb.udmb.system;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import javax.naming.InitialContext;

import org.apache.log4j.xml.DOMConfigurator;
import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import net.meddeb.bee.Beepin;


public class Init extends RemoteServiceServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(this.getClass());
	private static final String version = "0.8.0";
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Init() {
    super();
  }
	
	public void init(){
		System.out.println("--------------------------------------------------------------------");
		System.out.println("Starting UDMB module for uDir system, version " + version);
		System.out.println("--------------------------------------------------------------------");
		String prefix =  getServletContext().getRealPath("/");
		System.out.println("UDMB Init: prefix="+prefix);
		String file = getInitParameter("log4j-init-file");
		if (file != null) {
			System.out.println("UDMB Init: Configure log4j from "+prefix+file);
			DOMConfigurator.configure(prefix+file);
			logger.info("-------------------------------------------------------");
			logger.info("Starting UDMB module for uDir system, version " + version);
			logger.info("-------------------------------------------------------");
		} else {
			System.out.println("UDMB Init - FATAL:  no log4j-init-file parameter found, please fix it !");
		}
		try {
			InitialContext context = new InitialContext();
			Beepin beepin = (Beepin)context.lookup("java:global/udmb/Beepin");
			logger.debug("UDMB Beepin initialized: " + beepin);
			beepin.initialize("UDMB");
			Parameters udmbParams = (Parameters)context.lookup("java:global/udmb/Parameters");
			logger.debug("UDMB parameters initialized: " + udmbParams);
			for (int i=0; i<udmbParams.channelCount(); i++){
				beepin.addChanel(udmbParams.getChannelID(i), udmbParams.getChannelType(i), udmbParams.getMessageTypes(i));
			}
		} catch (Exception e){
			logger.error(e.getMessage());
		}
	}
}
