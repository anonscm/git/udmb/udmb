<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses.
--------------------------------------------------------------------->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>UDMB - Unified directory messages broker</title>
  <link type="text/css" rel="stylesheet" href="udmb.css">
  <script type="text/javascript" language="javascript" src="udmb/udmb.nocache.js"></script>
</head>
<body>
    <!-- RECOMMENDED if your web app will not function without JavaScript enabled -->
    <noscript>
      <div style="width: 22em; position: absolute; left: 50%; margin-left: -11em; color: red; background-color: white; border: 1px solid red; padding: 4px; font-family: sans-serif">
        Your web browser must have JavaScript enabled
        in order for this application to display correctly.
      </div>
    </noscript>
    <div class="header">
    	<div class="logobox">
    		<div class="titletext"><img src="res/udir-logo-64.png"/><br/>unified directory</div>
    	</div>
    	<div class="headerbox">
    		<div class="titlebox">
    			<div class="titletext">
    				UDMB - Administration tools
    			</div>
    		</div>
    		<!--div class="connectbox" id="connectPanelContainer">
    			< div id="connectPanelContainer"></div >
    		</div-->
    	</div>
    </div>	
    <div class="mainbox">
    	<div class="menubox">
    		<div id="menuContainer"></div>
    	</div>
    	<div class="contentbox">
    		<div id="brokerinfoContainer"></div>
    		<div class="sectionTitle">Available channels</div>
    		<div id="channelsinfoContainer"></div>
    		<!--  table align="center" border="1" cellpadding="2" cellspacing="0" width="100%" style="border-collapse:collapse;">
       		<tr align="center" style="background-color:darkblue; color:white;font-weight: bold;height:25px;">
          	<td width="15%">ID</td>
            <td width="30%">Topic</td>
          	<td width="8%">Type</td>
            <td width="25%">Reachable</td>
            <td width="22%">Status</td>
          </tr>
          
       		<tr align="left" style="height:25px;">
          	<td>pwdQualityParams</td>
            <td>Password quality parameters</td>
          	<td>External</td>
            <td id="pwdqpURIContainer"></td>
            <td id="pwdqpStatusContainer"></td>
          </tr>
    		</table -->
    	
    	</div>
    </div>
    <div class="footer">
    	<a href="http://www.meddeb.net/udmb" target="_blank" title="UDMB web site">UDMB for uDir, version 0.8.0 © 2014 AM.</a>
    </div>
</body>
</html>
