package net.meddeb.udmb.server;


import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import net.meddeb.bee.Beepin;
import net.meddeb.udmb.client.BrokerstatusService;
import net.meddeb.udmb.shared.BrokerStatus;
import net.meddeb.udmb.shared.ChannelInfo;
import net.meddeb.udmb.system.ChannelTester;
import net.meddeb.udmb.system.Parameters;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class BrokerstatusServiceImpl extends RemoteServiceServlet implements
		BrokerstatusService {
	private Logger logger = Logger.getLogger(BrokerstatusServiceImpl.class);

	@SuppressWarnings("finally")
	public BrokerStatus getBrokerstatus() throws IllegalArgumentException {
		BrokerStatus bs = new BrokerStatus();
		try {
			InitialContext context = new InitialContext();
			Beepin beepin = (Beepin)context.lookup("java:global/udmb/Beepin");
			logger.debug("Beepin successfully initialized");
			Parameters udmbParams = (Parameters)context.lookup("java:global/udmb/Parameters");
			logger.debug("Parameters successfully initialized");
			boolean localBrokerStarted = beepin.isLocalBrokerStarted();
			boolean foreignBrokerStarted = beepin.isForeignBrokerStarted();
			logger.debug("Parameters: foreign started: " + foreignBrokerStarted);
			bs.setLocalStarted(localBrokerStarted);
			bs.setForeignStarted(foreignBrokerStarted);
			bs.setAddress(beepin.getAddress());
			logger.debug("Parameters: address: " + bs.getAddress());
			if ((localBrokerStarted)||(foreignBrokerStarted)){
				ChannelInfo channelInfo = null;
				for (int i=0; i<udmbParams.channelCount(); i++){
					channelInfo = udmbParams.getChannelInfo(i);
					if ((localBrokerStarted)&&(channelInfo.isLocal())){
						channelInfo.setStarted(beepin.isChannelStarted(channelInfo.getID()));
						logger.debug("Parameters: add local channelInfo: " + channelInfo.getID());
						bs.addLocalChannelStatus(channelInfo);
					}
					if ((foreignBrokerStarted)&&(!channelInfo.isLocal())){
						channelInfo.setStarted(beepin.isChannelStarted(channelInfo.getID()));
						logger.debug("Parameters: add foreign channelInfo: " + channelInfo.getID());
						bs.addForeignChannelStatus(channelInfo);
					}
				}
			}
		} catch (Exception e){
			logger.error(e.getMessage());
		} finally {
			return bs;
		}
		
		
	}

	@SuppressWarnings("finally")
	@Override
	public boolean getChannelstatus(String channelID) throws IllegalArgumentException {
		boolean rslt = false;
		InitialContext context = null;
		ChannelTester channelTester = null;
		try {
			context = new InitialContext();
			logger.debug("Make a test for channel " + channelID);
			channelTester = (ChannelTester)context.lookup("java:global/udmb/ChannelTester");
			rslt = channelTester.isChannelWork(channelID);
			if (rslt) rslt = channelTester.isMessageReceived();
		} catch (NamingException e) {
			logger.error(e.getMessage());
		} finally{
			return rslt;
		}
	}
}
