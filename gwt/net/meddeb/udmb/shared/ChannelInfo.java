package net.meddeb.udmb.shared;

import java.io.Serializable;

public class ChannelInfo implements Serializable{
	private static final long serialVersionUID = 3588199172546601232L;
	private String ID = "";
	private String description = "";
	private String[] messageTypes = {}; 
	private int channelType = -1;
	private boolean local = false;
	private boolean started = false;
	public ChannelInfo() {
	}
	
	public void setID(String iD) {
		ID = iD;
	}

	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isStarted() {
		return started;
	}
	public void setStarted(boolean started) {
		this.started = started;
	}
	public String getID() {
		return ID;
	}
	public String[] getMessageTypes() {
		return messageTypes;
	}
	public void setMessageTypes(String[] messageTypes) {
		this.messageTypes = messageTypes;
	}
	public int getChannelType() {
		return channelType;
	}
	public void setChannelType(int channelType) {
		this.channelType = channelType;
	}
	public boolean isLocal() {
		return local;
	}
	public void setLocal(boolean local) {
		this.local = local;
	}
	
}
