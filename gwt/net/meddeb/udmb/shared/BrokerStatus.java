package net.meddeb.udmb.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class BrokerStatus implements Serializable {
	private static final long serialVersionUID = 8980046251176809714L;
	private boolean localStarted = false;
	private boolean foreignStarted = false;
	private String address = "";
	private boolean localCurrent = false;
	private int channelTesting = -1;
	private ArrayList<ChannelInfo> localChannelList = null;
	private ArrayList<ChannelInfo> foreignChannelList = null;

	public BrokerStatus() {
		localChannelList = new ArrayList<ChannelInfo>();
		foreignChannelList = new ArrayList<ChannelInfo>();
	}
	public boolean isLocalStarted() {
		return localStarted;
	}
	public void setLocalStarted(boolean started) {
		this.localStarted = started;
	}
	public boolean isForeignStarted() {
		return foreignStarted;
	}
	public void setForeignStarted(boolean started) {
		this.foreignStarted = started;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public boolean isLocalCurrent() {
		return localCurrent;
	}
	public void setLocalCurrent(boolean localCurrent) {
		this.localCurrent = localCurrent;
	}
	public int getChannelTesting() {
		return channelTesting;
	}
	public void setChannelTesting(int channelTesting) {
		this.channelTesting = channelTesting;
	}
	public int localChannelCount(){
		return localChannelList.size();
	}
	
	public ChannelInfo getLocalChannelStatus(int idx){
		return localChannelList.get(idx);
	}
	
	public void addLocalChannelStatus(ChannelInfo channelInfo){
		localChannelList.add(channelInfo);
	}
	
	public int foreignChannelCount(){
		return foreignChannelList.size();
	}
	
	public ChannelInfo getForeignChannelStatus(int idx){
		return foreignChannelList.get(idx);
	}
	
	public void addForeignChannelStatus(ChannelInfo channelInfo){
		foreignChannelList.add(channelInfo);
	}

}
