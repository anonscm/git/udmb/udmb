package net.meddeb.udmb.client;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;

import net.meddeb.udmb.shared.*;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Udmb implements EntryPoint {

	private boolean loaded = false;
	private BrokerStatus status;
	private final BrokerstatusServiceAsync brokerStatusService = GWT.create(BrokerstatusService.class);

	private final Grid channelsGrid = new Grid(1,5);
	private final Label lblRshBrokerValue = new Label("VM internal");
	private final Label lblStsBrokerValue = new Label("Running");
	
	private ClickHandler testClickHandler = new ClickHandler(){

		@Override
		public void onClick(ClickEvent event) {
			Cell cell = channelsGrid.getCellForEvent(event);
			if (cell != null){
				int row = cell.getRowIndex();
				status.setChannelTesting(row);
				final String channelID = channelsGrid.getText(row, 0);
				channelsGrid.setText(row, 3, "Testing");
				channelsGrid.getCellFormatter().setStyleName(row, 3, "testing");
				Timer timer = new Timer() {
					public void run() {
						isChannelWork(channelID);
					}
				};
				timer.schedule(800); 
			}
		}
		
	};
	private void initChannelGridHeader(){
		channelsGrid.setStyleName("allGrid");
		channelsGrid.getRowFormatter().setStyleName(0, "headerGrid");
		channelsGrid.getColumnFormatter().setWidth(0,"20%");
		channelsGrid.getColumnFormatter().setWidth(1,"50%");
		channelsGrid.getColumnFormatter().setWidth(2,"10%");
		channelsGrid.getColumnFormatter().setWidth(3,"10%");
		channelsGrid.getColumnFormatter().setWidth(4,"10%");
		channelsGrid.setText(0, 0, "ID");
		channelsGrid.setText(0, 1, "Description");
		channelsGrid.setText(0, 2, "Subscribers");
		channelsGrid.setText(0, 3, "Status");
		channelsGrid.setText(0, 4, "Test");
	}
	private void fillChannelGrid(boolean local){
		PushButton testButton;
		for (int i=channelsGrid.getRowCount()-1; i>0; i--){
			channelsGrid.removeRow(i);
		}
		if (local){
			if (!status.isLocalStarted()) return;
			for (int i=0; i<status.localChannelCount(); i++){
				channelsGrid.resizeRows(i+2);
				channelsGrid.setText(i+1, 0, status.getLocalChannelStatus(i).getID());
				channelsGrid.setText(i+1, 1, status.getLocalChannelStatus(i).getDescription());
				channelsGrid.setText(i+1, 2, "0");
				if (status.getLocalChannelStatus(i).isStarted()){
					channelsGrid.setText(i+1, 3, "Running");
					channelsGrid.getCellFormatter().setStyleName(i+1, 3, "running");
				} else {
					channelsGrid.setText(i+1, 3, "Stopped");
					channelsGrid.getCellFormatter().setStyleName(i+1, 3, "stopped");
				}
				testButton = new PushButton("Launch");
				testButton.addClickHandler(testClickHandler);
				channelsGrid.setWidget(i+1, 4, testButton);
			}
		}else {
			if (!status.isForeignStarted()) return;
			for (int i=0; i<status.foreignChannelCount(); i++){
				channelsGrid.resizeRows(i+2);
				channelsGrid.setText(i+1, 0, status.getForeignChannelStatus(i).getID());
				channelsGrid.setText(i+1, 1, status.getForeignChannelStatus(i).getDescription());
				channelsGrid.setText(i+1, 2, "0");
				if (status.getForeignChannelStatus(i).isStarted()){
					channelsGrid.setText(i+1, 3, "Running");
					channelsGrid.getCellFormatter().setStyleName(i+1, 3, "running");
				}else{
					channelsGrid.setText(i+1, 3, "Stopped");
					channelsGrid.getCellFormatter().setStyleName(i+1, 3, "stopped");
				}
				testButton = new PushButton("Launch");
				testButton.addClickHandler(testClickHandler);
				channelsGrid.setWidget(i+1, 4, testButton);
			}
		}
		status.setLocalCurrent(local);
	}
	
	private void setBrokerStatValue(boolean local){
		String brkAddress = "";
		String brkStatus = "";
		String stsStyleName = "running";
		if (local){
			brkAddress = "VM internal";
			if (status.isLocalStarted()){
				brkStatus = "Running";
			}
			else {
				brkStatus = "Stopped";
				stsStyleName = "stopped";
			}
		}else{
			brkAddress = status.getAddress();
			if (status.isForeignStarted()) {
				brkStatus = "Running";
			}
			else {
				brkStatus = "Stopped";
				stsStyleName = "stopped";
			}
		}
		lblRshBrokerValue.setText(brkAddress);
		lblStsBrokerValue.setText(brkStatus);
		lblStsBrokerValue.setStyleName(stsStyleName);

	}
	private void getChannelsInformation(){
		brokerStatusService.getBrokerstatus(new AsyncCallback<BrokerStatus>() {

			public void onFailure(Throwable caught) {
				System.out.println("Erreur retour serveur !!");
			}

			@Override
			public void onSuccess(BrokerStatus result) {
				status = result;
				fillChannelGrid(true);
			}
		});
	}
	private void isChannelWork(String channelID){
		brokerStatusService.getChannelstatus(channelID, new AsyncCallback<Boolean>(){

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Erreur retour serveur !!");
			}

			@Override
			public void onSuccess(Boolean result) {
				int row = status.getChannelTesting();
				if (status.isLocalCurrent()){
					status.getLocalChannelStatus(row-1).setStarted(result);
				} else {
					status.getForeignChannelStatus(row-1).setStarted(result);
				}
				if (result){
					channelsGrid.setText(row, 3, "Running");
					channelsGrid.getCellFormatter().setStyleName(row, 3, "running");
				}else{
					channelsGrid.setText(row, 3, "Stopped");
					channelsGrid.getCellFormatter().setStyleName(row, 3, "stopped");
				}
			}
			
		});
	}
	

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		if (loaded) return;
		final ToggleButton localBrokerButton = new ToggleButton("Local communications messaging broker");
		final ToggleButton foreignBrokerButton = new ToggleButton("Foreign communications messaging broker");
		//menu, 2 buttons
		localBrokerButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean down = foreignBrokerButton.isDown();
				down = !down;
				foreignBrokerButton.setDown(down);
				fillChannelGrid(localBrokerButton.isDown());
				setBrokerStatValue(localBrokerButton.isDown());
			}
		});
		foreignBrokerButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean down = localBrokerButton.isDown();
				down = !down;
				localBrokerButton.setDown(down);
				fillChannelGrid(localBrokerButton.isDown());
				setBrokerStatValue(localBrokerButton.isDown());
			}
		});
		localBrokerButton.setDown(true);
		final VerticalPanel brokersPanel = new VerticalPanel();
		brokersPanel.setSpacing(5);
		brokersPanel.add(localBrokerButton);
		brokersPanel.add(foreignBrokerButton);
		brokersPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		brokersPanel.addStyleName("verticalPan");
		//brokers info
		final Label lblRshBrokerTitle = new Label("Reachable at: ");
		lblRshBrokerTitle.setStyleName("labelTitle");
		lblRshBrokerValue.setStyleName("labelValue");
		final HorizontalPanel brokersInfoPanel1 = new HorizontalPanel();
		brokersInfoPanel1.setSpacing(5);
		brokersInfoPanel1.add(lblRshBrokerTitle);
		brokersInfoPanel1.add(lblRshBrokerValue);

		final Label lblStsBrokerTitle = new Label("Status: ");
		lblStsBrokerTitle.setStyleName("labelTitle");
		lblStsBrokerValue.setStyleName("labelValue");
		final HorizontalPanel brokersInfoPanel2 = new HorizontalPanel();
		brokersInfoPanel2.setSpacing(5);
		brokersInfoPanel2.add(lblStsBrokerTitle);
		brokersInfoPanel2.add(lblStsBrokerValue);
		final VerticalPanel brokersInfoPanel = new VerticalPanel();
		brokersInfoPanel.setSpacing(5);
		brokersInfoPanel.add(brokersInfoPanel1);
		brokersInfoPanel.add(brokersInfoPanel2);

		//channels
		initChannelGridHeader();
		getChannelsInformation();
		
		//channelsGrid.getCellFormatter().setStyleName(1, 3, "running");
		/*
		testButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				channelsGrid.setText(1, 3, "Testing");
				channelsGrid.getCellFormatter().setStyleName(1, 3, "testing");
				Timer timer = new Timer() {
					public void run() {
						goTest();
					}
				};
				timer.schedule(1000); 
			}
		
			
			
		MenuBar menu = new MenuBar();
	   menu.setAutoOpen(true);
	   menu.setWidth("100px");
	   menu.setAnimationEnabled(true);
	  MenuBar menuConnect = new MenuBar(); 

	   // Create the file menu
	   MenuBar fileMenu = new MenuBar(true);
	   fileMenu.setAnimationEnabled(true);

	   fileMenu.addItem("New", new Command() {
	      @Override
	      public void execute() {
	         showSelectedMenuItem("New");
	      }
	   });
	   fileMenu.addItem("Open", new Command() {
	      @Override
	      public void execute() {
	         showSelectedMenuItem("Open");
	      }
	   });
	   fileMenu.addSeparator();
	   fileMenu.addItem("Exit", new Command() {
	      @Override
	      public void execute() {
	         showSelectedMenuItem("Exit");
	      }
	   });
	   MenuBar editMenu = new MenuBar(true);
	   editMenu.setAnimationEnabled(true);

	   editMenu.addItem("Copy", new Command() {
	      @Override
	      public void execute() {
	         showSelectedMenuItem("Copy");
	      }
	   });
	   editMenu.addItem("Past", new Command() {
	      @Override
	      public void execute() {
	         showSelectedMenuItem("Past");
	      }
	   });*/

		// We can add style names to widgets
		//connectButton.addStyleName("menuButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("menuContainer").add(brokersPanel);
		RootPanel.get("brokerinfoContainer").add(brokersInfoPanel);
		RootPanel.get("channelsinfoContainer").add(channelsGrid);


		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		loaded = true;
	}
}
