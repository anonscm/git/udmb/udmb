package net.meddeb.udmb.client;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import net.meddeb.udmb.shared.BrokerStatus;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>BrokerstatusService</code>.
 */
public interface BrokerstatusServiceAsync {
	void getBrokerstatus(AsyncCallback<BrokerStatus> asyncCallback)
			throws IllegalArgumentException;
	void getChannelstatus(String channelID, AsyncCallback<Boolean> asyncCallback)
			throws IllegalArgumentException;
}
