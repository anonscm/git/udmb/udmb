package net.meddeb.udmb.client;
/*--------------------------------------------------------------------
UDMB, Unified Directory messages broker for uDir
Messaging system management for uDir.
Copyright (C) 2014, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import net.meddeb.udmb.shared.BrokerStatus;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../greet")
public interface BrokerstatusService extends RemoteService {
	BrokerStatus getBrokerstatus() throws IllegalArgumentException;
	boolean getChannelstatus(String channelID) throws IllegalArgumentException;
}
